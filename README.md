PaccaTraka is an open source website. Its purpose is to support Alpaca farmer tracking stuff.

It's designed to be used in the field/showroom via a mobile device such as any modern smart phone, tablet or laptop.

# Features #

- Show Room Results Tracking

# Building this project #

- Install the following
* Java JDK
* Docker  (be sure to configure to be able to use 4GB of RAM or more)
* Maven 3

- Build and start the application
* Run "mvn install -Ddocker.keepRunning=true" and a docker image will be built and started

# Hacking the code #

- edit src files
- update unit tests
- deploy the changes to the running server
- Run "mvn install -Pslinginstall"
- Test via the GUI
- update integration tests in the hawksite.runtime module
- run integration tests
- stop the server "mvn pre-integration-test"